# -*- coding: utf-8 -*-

import os, sys
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterVectorLayer,
                       QgsVectorLayer,
                       QgsProject,
                       QgsFeatureRequest,
                       QgsProcessingContext)
from qgis import processing


class Perforation(QgsProcessingAlgorithm):
    # Constants used to refer to parameters and outputs
    INPUT_LM1 = 'Landscape Before'
    INPUT_LM2 = 'Landscape After'
    OUTPUT = 'Output Shapefile'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return Perforation()

    def name(self):
        return 'Perforation'

    def displayName(self):
        return self.tr('Perforation')

    def group(self):
        return self.tr('Perforation')

    def groupId(self):
        return 'Perforation'

    def shortHelpString(self):

        return self.tr("""The process of making holes in an object such as a habitat or land type (Forman, 1995). It is a particular case of “ToD E – Loss” in which the loss occurs inside the patch and there number of patches remains unchanged. 
        This tool calculates Perforation between two “Landscape Moments” shapefiles and creates a new shapefile that includes the perforation polygons. The analysis is conducted at the class level and does not use analytical units (squares or districts).\n 
        Landscape Moment 1:   Landscape (LULC category) shapefile in the earliest moment of analysis\n
        Landscape Moment 2:   Landscape (LULC category) shapefile in the second moment of analysis\n
        Output Shapefile:   Select the name of the output shapefile\n
        
        References: Forman, R.T.T., 1995. Land mosaics - The ecology of landscapes and regions. Cambridge University Press, Cambridge.""")
        

    def initAlgorithm(self, config=None):
        
        # Parameters
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM1,
                self.tr('Landscape Before'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM2,
                self.tr('Landscape After'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output Shapefile')
            )
        )
    
    def processAlgorithm(self, parameters, context, feedback):
        
        QgsProcessingContext().setInvalidGeometryCheck(QgsFeatureRequest.GeometrySkipInvalid)
        
        # Input and Output variables
        land1S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM1,
            context,
            ['shp']
        )
        land2S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM2,
            context,
            ['shp']
        )
        dest_id_name = self.parameterAsFileOutput(
            parameters,
            self.OUTPUT,
            context,
        )

        # Temporary Folder
        dirTemp = os.path.join(os.path.dirname(dest_id_name), "tempFolderLDT")
        if not os.path.exists(dirTemp):
            os.makedirs(dirTemp)
        else:
            for root, dirs, files in os.walk(dirTemp):
                for file in files:
                    path = os.path.join(dirTemp, file)
                    os.remove(path)
            feedback.pushInfo("Temporary folder already exists")
        
        #Buffer 0
        inlyrL1 = QgsVectorLayer(land1S, 'temp', 'ogr')
        inlyrL2 = QgsVectorLayer(land2S, 'temp', 'ogr')

        land1S = os.path.join(dirTemp, os.path.basename(land1S))
        land2S = os.path.join(dirTemp, os.path.basename(land2S))

        processing.run("qgis:buffer", {"INPUT": inlyrL1, "DISTANCE": 0, "OUTPUT": land1S})
        processing.run("qgis:buffer", {"INPUT": inlyrL2, "DISTANCE": 0, "OUTPUT": land2S})

        del inlyrL1, inlyrL2
           
        # Temporary Shapefiles
        symdif = os.path.join(dirTemp, "symdif.shp")
        ms_symdif = os.path.join(dirTemp, "mu_symdif.shp")
        pref = os.path.join(dirTemp, "pref.shp")
        prefBuff = os.path.join(dirTemp, "prefBuff.shp")

        
        # Functions
        def multi_to_single(input_shp, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:multiparttosingleparts", {"INPUT": input_shp, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)

        def symmetrical_difference(input_shp, overlay, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:symmetricaldifference", {"INPUT": input_shp, "OVERLAY": overlay, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)
        
        def selectPref(in_layer, intersect, buff, output):
            inlyr = QgsVectorLayer(in_layer, 'temp', 'ogr')
            inlyrI = QgsVectorLayer(intersect, 'temp', 'ogr')
            processing.run('native:buffer', {"INPUT":inlyr, "DISTANCE": 0.2, "OUTPUT": buff})
            del inlyr
            inlyr = QgsVectorLayer(buff, 'temp', 'ogr')
            #processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 3, "INTERSECT": inlyrI, "METHOD": 0})
            #processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 4, "INTERSECT": inlyrI, "METHOD": 1})
            #inlyr.invertSelection()
            processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 6, "INTERSECT": inlyrI, "METHOD": 0})
            processing.run('native:saveselectedfeatures', {"INPUT":inlyr, "OUTPUT": output})
            #driver = ogr.GetDriverByName('ESRI Shapefile')
            #outds = driver.CreateDataSource(output)
            #outlyr = outds.CopyLayer(inlyr, output)
            del inlyr, inlyrI, buff
        
        def isEmpty(input_shp):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            count = 0
            for feature in inlyr.getFeatures():
                count = count + 1
                if count > 0:
                    inlyr = None
                    return count == 0

            inlyr = None

            return count == 0


        feedback.pushInfo("Processing, please wait.")
        feedback.pushInfo("If a command window opens, do not close it. It will close automatically.")

        # Symmetrical Difference moment 1, 2 and 3
        symmetrical_difference(land1S, land2S, symdif)
        feedback.pushInfo("Symmetrical Difference Done")
        multisingle_symdif12 = multi_to_single(symdif, ms_symdif)
        feedback.pushInfo("Multi to SinglePart Done")
        
        selectPref(multisingle_symdif12, land1S, prefBuff, pref)
        feedback.pushInfo("Perforation Done")

        if(isEmpty(pref)):
            for root, dirs, files in os.walk(dirTemp):
                for file in files:
                    path = os.path.join(dirTemp, file)
                    os.remove(path)
            os.rmdir(dirTemp)

            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
            feedback.pushInfo("!!!    WARNING: NO PERFORATION FOUND    !!!")
            feedback.pushInfo("No output layer will be created !!!")
            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
            return {}
        

        layer = QgsVectorLayer(pref, "", "ogr")

        # Finish process
        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            layer.fields(),
            layer.wkbType(),
            layer.sourceCrs()
        )
        
        for f in layer.getFeatures():
            sink.addFeature(f, QgsFeatureSink.FastInsert)
           
        QgsProject.instance().removeMapLayer(layer.id())
        
        del layer

        for root, dirs, files in os.walk(dirTemp):
            for file in files:
                path = os.path.join(dirTemp, file)
                os.remove(path)
        os.rmdir(dirTemp)
        
        self.dest_id=dest_id
        
        return {self.OUTPUT: dest_id}
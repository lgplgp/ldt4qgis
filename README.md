## LDT4QGIS
(Version 1; 24/05/2022)
Name: LDT4QGIS
License: GNU GPLv3
Developers: Luis Paixão, Rui Machado
E-mails: luispaixao@agroinsider.com; rdpm@uevora.pt
Year first available: 2022
Software required: QGIS
Program language: Python


### Contents
"README.md" (this file, with details, descriptions and instructions)
"COPYING.txt" (License file)
"LDT4QGIS.py" (general script to calculate Landscape Dynamic Typology with tools 1 to 4)
"Perforation.py" (script to calculate perforation at the class scale between two analytical moments)
"GainedLostPatches.py" (script to identify and locate all the individual patches gained and lost between two analytical moments.)
"LDT4QGIS_2M_symbology.qml" (Layer file with symbology for output file in two moments analysis. Field to apply: ToD) 
"LDT4QGIS_3M_symbology.qml" (Layer file with symbology for output file in three moments analysis. Field to apply: ToD1_3)


### Description
LDT4QGIS is a set of Python scripts that implement the Landscape Dynamic Typology (LDT) method developed by Machado et al. 2018. The main purpose is to assess land use / land cover (LUCL) changes between two or three moments, considering the composition and configuration of a binary landscape.

The current version (version 1) consists of three scripts: 

I - "LDT4QGIS.py" - main script that runs the core LDT method, allowing different options:

    "1. Landscape Dynamic Types 2M (Squares)"; Implements the LDT method using two moments and squares as analytical units. 

    "2. Landscape Dynamic Types 3M (Squares)"; Implements the LDT method using three moments and squares as analytical units.  

    "3. Landscape Dynamic Types 2M (Districts)"; Implements the LDT method using two moments and distrits provided by the user as analytical units.

    "4. Landscape Dynamic Types 3M (Districts)"; Implements the LDT method using three moments and districts provided by the user as analytical units.

NOTE: There is an option to include the ToD "E1 - Perforation" in the analysis. It is optional because when it is assigned to an analytical unit, it does mean that it occurred but does not mean it was the only area loss that occurred, so extra caution is needed to avoid misinterpreting the results. 


II - "Perforation.py" - Accessory script to geoprocess, at the class scale, the spatial pattern "perforation", which is a particular case of the ToD "Loss". Perforation is relevant as it is one of the most common forms of spatial transformation that could lead to advanced forms of habitat loss and fragmentation (Forman RTT, 1995).

III - "GainedLostPatches.py" - Accessory script to identify and locate the gained and lost patches of the LULC category of interest between two analytical moments.  



___

### How to use LDT4QGIS
**Requirements and preliminary steps**

1. A projected (not geographic) coordinate reference system (CRS) should be used and the coordinates displayed in meters. All shapefiles should be in the same CRS system.
2. Add or create the landscape shapefiles to be analysed. LDT uses binary landscapes and thus the input land cover shapefiles must contain only one category with the polygons of interest.
3. Store the scripts in the QGIS script folder. The path to the folder is shown at: Processing Toolbox – Options – Processing – Scripts. Exit and restart QGIS so that the scripts are automatically loaded to the Processing Toolbox.
4. Two symbology files are provided and can be loaded and applied to the output files. If stored in the same folder along with the scripts the symbology is automatically applied. 


**Inputs and settings**

I - "LDT4QGIS.py":

- Moments: Choose whether the analysis should be based on two or three moments.
- Type of Analysis: Choose whether the analytical units should be squares or districts (Polygonal shapefile containing the districts’ boundaries, provided by the user.) 
- Study Area Polygon: Polygonal shapefile containing the study area boundaries.
- Landscape Moment 1: Polygonal shapefile of the landscape in moment 1.
- Landscape Moment 2: Polygonal shapefile of the landscape in moment 2.
- Landscape Moment 3: Polygonal shapefile of the landscape in moment 3.
- Keep patches equal or larger than (square meters): Minimum patch size to be analysed.
- Squares width and height (meters): Analytic square size. (Only for a square-based analysis)
- Perforation: Check the box to include the “ToD E1 – Perforation” to the analysis. 
- 'Forecast’ calculates a hypothetical scenario assuming the ongoing trends will persist. The forecast tool considers how the ToD can evolve from one to the other (Machado et al. 2018). In a three-moment analysis (3M) the field 'ToD_23' is used to set the most recent trend.
- Output Shapefile: Name and path of the output file.


II - "Perforation.py":

- Landscape Before: Polygonal shapefile of the landscape in the earliest moment of analysis.
- Landscape After: Polygonal shapefile of the landscape in the latest moment of analysis.
- Output Shapefile: Name and path of the output file.


III - "GainedLostPatches.py":

- Landscape Before: Polygonal shapefile of the landscape in the earliest moment of analysis.
- Landscape After: Polygonal shapefile of the landscape in the latest moment of analysis.
- Output Path: Select the folder in which the output shapefiles will be saved. 
- Output Name: Select a prefix to the output file names. The names are by default "GainedPatches", "LostPatches", "GainedArea" and "LostArea".



**Outputs**


I - "LDT4QGIS.py": The final product is a shapefile with the metrics calculated for the two or three dates and the districts or squares assigned to a Type of Dynamic. The provided symbology layers may help to produce a map afterwards.

II - "Perforation.py": The output is a shapefile containing the polygons representing the perforations occurred. 

III - "GainedLostPatches.py": The output comes in the form of four shapefiles. Regarding the individual patches there is one shapefile with the gained patches and other containing the lost patches. In order to extract these individual patches, intermediary shapefiles containing all the gained and lost areas (including partial patches variations) are computed. These shapefiles are also provided, one for area gains and one for area losses.


___


 
References:

Forman, R.T.T., 1995. Land mosaics - The ecology of landscapes and regions. Cambridge University Press, Cambridge.

Machado, R., Godinho, S., Pirnat, J., Neves, N., Santos, P., 2018. Assessment of landscape composition and configuration via spatial metrics combination: conceptual framework proposal and method improvement. Landsc. Res. 43, 652–664. https://doi.org/10.1080/01426397.2017.1336757

Related references:
Machado, R., Bayot, R., Godinho, S., Pirnat, J., Santos, P., & de Sousa-Neves, N. (2020). LDTtool: A toolbox to assess landscape dynamics. Environmental Modelling and Software, 133(August). https://doi.org/10.1016/j.envsoft.2020.104847

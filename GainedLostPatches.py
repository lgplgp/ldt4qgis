# -*- coding: utf-8 -*-

import os, sys
# import ogr
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterFolderDestination,
                       QgsProcessingParameterString,
                       QgsProcessingContext,
                       QgsVectorLayer,
                       QgsProject,
                       QgsFeatureRequest,
                       QgsProcessingContext,
                       QgsField,
                       QgsExpression,
                       QgsVectorFileWriter)
from PyQt5.QtCore import QVariant
from qgis import processing
from qgis.utils import iface


class GainLostPatches(QgsProcessingAlgorithm):
    # Constants used to refer to parameters and outputs
    INPUT_LM1 = 'Landscape Before'
    INPUT_LM2 = 'Landscape After'
    FOLD = "Output Path"
    OUTPUT = 'Output Name'
    GAINA = "Gained Area"
    LOSTA = "Lost Area"
    GAINP = "Gained Patches"
    LOSTP = "Lost Patches"
    
    #show layers
    layers = []
    #remove layers
    remove_layers = []
    dir_r = ""

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return GainLostPatches()

    def name(self):
        return 'Gained and Lost Patches'

    def displayName(self):
        return self.tr('Gained and Lost Patches')

    def group(self):
        return self.tr('Gained and Lost Patches')

    def groupId(self):
        return 'Gained and Lost Patches'

    def shortHelpString(self):

        return self.tr("""Accessory script to identify and locate the places where amounts of the LULC category of interest were gained and lost between two analytical moments, including new individual patches or individual patches that disappeared. 
        It produces four output shapefiles regarding: all gained areas, all lost areas, gained patches and lost patches.""")
        

    def initAlgorithm(self, config=None):
        
        # Parameters
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM1,
                self.tr('Landscape Before'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM2,
                self.tr('Landscape After'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.FOLD,
                self.tr('Output Path')
            )
        )
        self.addParameter(
            QgsProcessingParameterString(
                self.OUTPUT,
                self.tr('Output Name')
            )
        )
        
    
    def processAlgorithm(self, parameters, context, feedback):
        
        QgsProcessingContext().setInvalidGeometryCheck(QgsFeatureRequest.GeometrySkipInvalid)
        
        # Input and Output variables
        land1S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM1,
            context,
            ['shp']
        )
        land2S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM2,
            context,
            ['shp']
        )
        
        dest_fold = self.parameterAsString(
            parameters,
            self.FOLD,
            context,
        )
        self.dir_r = dest_fold
        
        dest_id_name = self.parameterAsString(
            parameters,
            self.OUTPUT,
            context,
        )

        # Temporary Folder
        dirTemp = os.path.join(dest_fold, "tempFolderLDT")
        if not os.path.exists(dirTemp):
            os.makedirs(dirTemp)
        else:
            for root, dirs, files in os.walk(dirTemp):
                for file in files:
                    path = os.path.join(dirTemp, file)
                    os.remove(path)
            feedback.pushInfo("Temporary folder already exists")
        
        #Buffer 0
        inlyrL1 = QgsVectorLayer(land1S, 'temp', 'ogr')
        inlyrL2 = QgsVectorLayer(land2S, 'temp', 'ogr')

        land1S = os.path.join(dirTemp, "Before.shp")
        land2S = os.path.join(dirTemp, "After.shp")

        processing.run("qgis:buffer", {"INPUT": inlyrL1, "DISTANCE": 0, "OUTPUT": land1S})
        processing.run("qgis:buffer", {"INPUT": inlyrL2, "DISTANCE": 0, "OUTPUT": land2S})

        del inlyrL1, inlyrL2
        
        # Fields
        Before_Field = "FID_Before"
        After_Field = "FID_After"
        
        # Temporary Shapefiles
        symdif = os.path.join(dirTemp, "symdif.shp")
        ms_symdif = os.path.join(dirTemp, "mu_symdif.shp")
        lostA = os.path.join(dest_fold, dest_id_name + "_LostArea.shp")
        gainA = os.path.join(dest_fold, dest_id_name + "_GainedArea.shp")
        lostP = os.path.join(dest_fold, dest_id_name + "_LostPatches.shp")
        gainP = os.path.join(dest_fold, dest_id_name + "_GainedPatches.shp")

        
        # Functions
        def multi_to_single(input_shp, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:multiparttosingleparts", {"INPUT": input_shp, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)

        def symmetrical_difference(input_shp, overlay, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:symmetricaldifference", {"INPUT": input_shp, "OVERLAY": overlay, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)
        
        def create_field(input_shp, field_name, field_type, field_precision, field_width):

            if field_type == 0:
                new_field = QgsField(field_name, QVariant.Int, len = field_width)
            elif field_type == 1:
                new_field = QgsField(field_name, QVariant.Double, len = field_width, prec = field_precision)
            elif field_type == 2:
                new_field = QgsField(field_name, QVariant.String, len = field_width)

            layer = QgsVectorLayer(input_shp, "input", "ogr")

            if not layer.fields().indexFromName(field_name) >= 0:
                
                layer.dataProvider().addAttributes([new_field])
                layer.updateFields()

        def calcBeforeAfterField(input_shp, target_before, target_after, target_value, source_before, source_after):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            inlyr.startEditing()
            for feature in inlyr.getFeatures():

                #inlyr.startEditing()
                #feature.setAttribute(feature.fieldNameIndex(target_field), target_value)
                #inlyr.updateFeature(feature)
                feature[target_before] = target_value
                feature[target_after] = target_value

                pos = feature[source_before]
                if not feature[source_before] and not feature[source_after]:
                    feature[target_before] = feature[source_before]
                else:
                    if not pos:
                        pass
                    else:
                        feature[target_before] = feature[source_before]
                        #feature.setAttribute(feature.fieldNameIndex(fieldNULL), feature.attributes()[feature.fieldNameIndex(fieldCalc)] )
                    pos = feature[source_after]
                    if not pos:
                        pass
                    else:
                        feature[target_after] = feature[source_after]
                    
                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None
        
            
        def select_att(input_shp, field, operator, value, output_shp):

            select = ("{field1}{operator1}{value1}").format(
                field1 = field,
                operator1 = operator,
                value1 = value
            )

            layer = QgsVectorLayer(input_shp, "input", "ogr")

            expression = QgsExpression(select)
            request = QgsFeatureRequest(expression)

            layer.removeSelection()

            matching_features = layer.getFeatures(request)
            feature_ids = [feature.id() for feature in matching_features]
            layer.selectByIds(feature_ids)


            new_layer = QgsVectorLayer("Polygon?crs=" + layer.crs().authid(), "Selected Features", "memory")
            new_layer_data = new_layer.dataProvider()

            new_layer_data.addAttributes(layer.fields())
            new_layer.updateFields()

            for feature in layer.getSelectedFeatures():
                new_layer_data.addFeature(feature)

            QgsVectorFileWriter.writeAsVectorFormat(new_layer, output_shp, "UTF-8", layer.crs(), "ESRI Shapefile")

            layer.removeSelection()
            
            return(output_shp)
            
        def export_byLocation(in_layer, intersect, output_shp):
            inlyr = QgsVectorLayer(in_layer, 'temp', 'ogr')
            inlyrI = QgsVectorLayer(intersect, 'temp', 'ogr')
            
            inlyr.selectAll()
            
            processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 0, "INTERSECT": inlyrI, "METHOD": 3})
            processing.run('native:saveselectedfeatures', {"INPUT": inlyr, "OUTPUT": output_shp} )

            inlyr = None
            inlyrI = None
            del inlyr, inlyrI
            
        def isEmpty(input_shp):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            count = 0
            for feature in inlyr.getFeatures():
                count = count + 1
                if count > 0:
                    inlyr = None
                    return count == 0

            inlyr = None

            return count == 0


        
        
        feedback.pushInfo("Processing, please wait.")
        feedback.pushInfo("If a command window opens, do not close it. It will close automatically.")

        # Symmetrical Difference moment 1 and 2
        symmetrical_difference(land1S, land2S, symdif)
        feedback.pushInfo("Symmetrical Difference Done")
        multisingle_symdif12 = multi_to_single(symdif, ms_symdif)
        feedback.pushInfo("Multi to SinglePart Done")
        
        # Create and calculate FID fields
        create_field(multisingle_symdif12, Before_Field, 0, 0, 10)
        create_field(multisingle_symdif12, After_Field, 0, 0, 10)
        calcBeforeAfterField(multisingle_symdif12, Before_Field, After_Field, -1, "OBJECTID", "OBJECTID_2")
        
        select_att(multisingle_symdif12, After_Field, "=", -1, lostA)
        select_att(multisingle_symdif12, Before_Field, "=", -1, gainA)
        export_byLocation(lostA, land2S, lostP)
        export_byLocation(gainA, land1S, gainP)
        
        if isEmpty(lostA):
            self.remove_layers.append(lostA)

            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
            feedback.pushInfo("!!!    WARNING: NO LOST AREAS FOUND    !!!")
            feedback.pushInfo("No output layer will be created !!!")
            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
        else:
            self.layers.append(lostA)
            
        if isEmpty(gainA):
            self.remove_layers.append(gainA)

            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
            feedback.pushInfo("!!!    WARNING: NO GAINED AREAS FOUND    !!!")
            feedback.pushInfo("No output layer will be created !!!")
            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
        else:
            self.layers.append(gainA)
            
        if isEmpty(lostP):
            self.remove_layers.append(lostP)

            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
            feedback.pushInfo("!!!    WARNING: NO LOST PATCHES FOUND    !!!")
            feedback.pushInfo("No output layer will be created !!!")
            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
        else:
            self.layers.append(lostP)
            
        if isEmpty(gainP):
            self.remove_layers.append(gainP)

            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
            feedback.pushInfo("!!!    WARNING: NO GAINED PATCHES FOUND    !!!")
            feedback.pushInfo("No output layer will be created !!!")
            feedback.pushInfo(" ")
            feedback.pushInfo(" ")
        else:
            self.layers.append(gainP)

        '''
        layer_lostA = QgsVectorLayer(lostA, "", "ogr")
        layer_gainA = QgsVectorLayer(gainA, "", "ogr")

        # Finish process
        (sink, dest_lostA) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            layer_lostA.fields(),
            layer_lostA.wkbType(),
            layer_lostA.sourceCrs()
        )
        for f in layer_lostA.getFeatures():
            sink.addFeature(f, QgsFeatureSink.FastInsert)
            
        (sink, dest_gainA) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            layer_gainA.fields(),
            layer_gainA.wkbType(),
            layer_gainA.sourceCrs()
        )
        for f in layer_gainA.getFeatures():
            sink.addFeature(f, QgsFeatureSink.FastInsert)
        
        
           
        QgsProject.instance().removeMapLayer(layer_lostA.id())
        QgsProject.instance().removeMapLayer(layer_gainA.id())
        
        del layer_lostA
        del layer_gainA
        '''
        
        for root, dirs, files in os.walk(dirTemp):
            for file in files:
                path = os.path.join(dirTemp, file)
                os.remove(path)
        os.rmdir(dirTemp)
        
        '''
        self.dest_lostA=dest_lostA
        self.dest_gainA=dest_gainA
        '''
        return {}
    
    def postProcessAlgorithm(self, context, feedback):
        
        def removeSHP(prefix, in_dir):
            for root, dirs, files in os.walk(in_dir):
                for file in files:
                    if file.startswith(prefix):
                        path = os.path.join(in_dir, file)
                        os.remove(path)
                        
        registry = QgsProject.instance()
        
        for i in self.remove_layers:
            removeSHP(os.path.splitext(os.path.basename(i))[0], self.dir_r)
        
        for i in self.layers:
            
            layer = QgsVectorLayer(i, os.path.basename(i),'ogr')
            registry.addMapLayer(layer)
        
        
        return {}
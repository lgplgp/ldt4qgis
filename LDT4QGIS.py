# -*- coding: utf-8 -*-

import os, sys
# import ogr
from math import ceil
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterEnum,
                       QgsVectorLayer,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsFeatureRequest,
                       QgsProcessingUtils,
                       QgsProcessingContext,
                       QgsPointXY,
                       QgsGeometry,
                       QgsFeature,
                       QgsVectorFileWriter,
                       QgsField,
                       QgsExpression)
from PyQt5.QtCore import QVariant
from qgis import processing
from qgis.utils import iface
from processing.core.ProcessingConfig import ProcessingConfig
from processing.tools import dataobjects


class LDT4QGIS(QgsProcessingAlgorithm):
    # Constants used to refer to parameters and outputs
    MOMENT = 'Moments'
    AREATYPE = 'AreaType'
    INPUT_SA = 'Study Polygon Area'
    INPUT_LM1 = 'Landscape Moment 1'
    INPUT_LM2 = 'Landscape Moment 2'
    INPUT_LM3 = 'Landscape Moment 3'
    INPUT_PATCH = 'Keep patches equal or larger than (sq. meters)'
    INPUT_SQDIM = 'Squares width and height (meters)'
    OUTPUT = 'Output Shapefile'
    PERFORATION = 'Perforation'
    FORECAST = 'Forecast'
    m3 = False
    dstr = False

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return LDT4QGIS()

    def name(self):
        return 'LDT4QGIS'

    def displayName(self):
        return self.tr('Landscape Dynamics Types')

    def group(self):
        return self.tr('LDT4QGIS')

    def groupId(self):
        return 'LDT4QGIS'

    def shortHelpString(self):

        return self.tr("""Calculates Landscape Dynamic Types for 2 or 3 moments, using squares or districts as analytical units.\n 
        Study Polygon Area:   Study area boundary\n
        Landscape Moment 1:   Landscape (LULC category) shapefile in the earliest moment of analysis\n
        Landscape Moment 2:   Landscape (LULC category) shapefile in the second moment of analysis\n
        Landscape Moment 3:   Landscape (LULC category) shapefile in the latest moment of analysis (only valid for 3 moments analysis)\n
        Keep patches equal or larger than (sq. meters):   Minimum patch size in square meters.(All the patches smaller than this threshold value will be discarded from the analysis)\n
        Squares width and height (meters):   Insert square size, width and height, in meters (only valid for squares as analytical unit)
        Perforation: The process of making holes in an object such as a habitat or land type (Forman, 1995). It is a particular case of “ToD E – Loss” in which the loss occurs inside the patch and there number of patches remains unchanged.
        'Forecast’ calculates a hypothetical scenario assuming the ongoing trends will persist. The forecast tool considers how the ToD can evolve from one to the other (Machado et al. 2018). In a three-moment analysis (3M) the field 'ToD_23' is used to set the most recent trend.
        Output Shapefile:   Select the name of the output shapefile\n
        
        References: Forman, R.T.T., 1995. Land mosaics - The ecology of landscapes and regions. Cambridge University Press, Cambridge.""")
        

    def initAlgorithm(self, config=None):
        
        # Parameters
        self.addParameter(
            QgsProcessingParameterEnum(
                self.MOMENT,
                self.tr('Moments'),
                ["2", "3"]
            )
            
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.AREATYPE,
                self.tr('Type of Analysis'),
                ["Squares", "Districts"]
            )
            
        )
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_SA,
                self.tr('Study Area Polygon'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM1,
                self.tr('Landscape Moment 1'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM2,
                self.tr('Landscape Moment 2'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.INPUT_LM3,
                self.tr('Landscape Moment 3 (Ignore if 2 Moment analysis is selected)'),
                [QgsProcessing.TypeVectorPolygon],
                optional = True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.INPUT_PATCH,
                self.tr('Keep patches equal or larger than (sq. meters)'),
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.INPUT_SQDIM,
                self.tr('Squares width and height (meters) (Ignore if Districts analysis is selected)'),
                optional = True
            )
        )
        self.addParameter(
            QgsProcessingParameterBoolean(
                self.PERFORATION,
                self.tr('Perforation'),
                defaultValue = False
            )
        )
        self.addParameter(
            QgsProcessingParameterBoolean(
                self.FORECAST,
                self.tr('Forecast'),
                defaultValue = False
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output Shapefile')
            )
        )
    
    def processAlgorithm(self, parameters, context, feedback):
        
        QgsProcessingContext().setInvalidGeometryCheck(QgsFeatureRequest.GeometrySkipInvalid)
        
        # Input and Output variables
        mmt = self.parameterAsString(
            parameters,
            self.MOMENT,
            context
        )
        atyp = self.parameterAsString(
            parameters,
            self.AREATYPE,
            context
        )
        study_areaS = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_SA,
            context,
            ['shp']
        )
        land1S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM1,
            context,
            ['shp']
        )
        land2S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM2,
            context,
            ['shp']
        )
        land3S = self.parameterAsCompatibleSourceLayerPath(
            parameters,
            self.INPUT_LM3,
            context,
            ['shp']
        )
        min_size = self.parameterAsInt(
            parameters,
            self.INPUT_PATCH,
            context
        )
        squares = self.parameterAsInt(
            parameters,
            self.INPUT_SQDIM,
            context
        )
        pref = self.parameterAsBool(
            parameters,
            self.PERFORATION,
            context
        )
        forec = self.parameterAsBool(
            parameters,
            self.FORECAST,
            context
        )
        dest_id_name = self.parameterAsFileOutput(
            parameters,
            self.OUTPUT,
            context,
        )

        # configCheck
        #m3 = False
        if mmt == "1":
            self.m3 = True
        #dstr = False
        if atyp == "1":
            self.dstr = True

        
        # Temporary Folder
        dirTemp = os.path.join(os.path.dirname(dest_id_name), "tempFolderLDT")
        if not os.path.exists(dirTemp):
            os.makedirs(dirTemp)
        else:
            for root, dirs, files in os.walk(dirTemp):
                for file in files:
                    path = os.path.join(dirTemp, file)
                    os.remove(path)
            feedback.pushInfo("Temporary folder already exists")
        
        #Buffer 0
        inlyrSD = QgsVectorLayer(study_areaS, 'temp', 'ogr')
        inlyrL1 = QgsVectorLayer(land1S, 'temp', 'ogr')
        inlyrL2 = QgsVectorLayer(land2S, 'temp', 'ogr')

        study_areaS = os.path.join(dirTemp, os.path.basename(study_areaS))
        land1S = os.path.join(dirTemp, os.path.basename(land1S))
        land2S = os.path.join(dirTemp, os.path.basename(land2S))
        
        processing.run("qgis:buffer", {"INPUT": inlyrSD, "DISTANCE": 0, "OUTPUT": study_areaS})
        processing.run("qgis:buffer", {"INPUT": inlyrL1, "DISTANCE": 0, "OUTPUT": land1S})
        processing.run("qgis:buffer", {"INPUT": inlyrL2, "DISTANCE": 0, "OUTPUT": land2S})

        del inlyrSD, inlyrL1, inlyrL2
        
        #feedback.pushInfo(m3)
        if self.m3:
            inlyrL3 = QgsVectorLayer(land3S, 'temp', 'ogr')
            land3S = os.path.join(dirTemp, os.path.basename(land3S))
            processing.run("qgis:buffer", {"INPUT": inlyrL3, "DISTANCE": 0, "OUTPUT": land3S})
            del inlyrL3

        # Fields
        Fid_Field = "FID"
        areaInicial_field = "Area"
        freq_field_1 = "NP1"
        freq_field_2 = "NP2" 
        area_1 = "area_1"
        area_2 = "area_2"
        presence_1 = "Presence1"
        presence_2 = "Presence2"
        var_area_12 = "varArea_12"
        var_NP_12 = "var_NP_12"
        ToD_12 = "ToD_12"
        sym12 = "symdif_12"
        perforation12 = "Perf_12"
        
        if self.m3:
            freq_field_3 = "NP3"
            area_3 = "area_3"
            presence_3 = "Presence3"
            var_area_23 = "varArea_23"
            var_area_13 = "varArea_13"
            var_NP_23 = "var_NP_23"
            var_NP_13 = "var_NP_13"
            ToD_23 = "ToD_23"
            ToD_13 = "ToD_13"
            sym23 = "symdif_23"
            sym13 = "symdif_13"
            perforation23 = "Perf_23"
            perforation13 = "Perf_13"
        
        forecast = "ToD_For"

        # Temporary Shapefiles
        fishnet_temp1 = os.path.join(dirTemp, "fishnet_t1.shp")
        fishnet_temp2 = os.path.join(dirTemp, "fishnet_t2.shp")
        intersect_temp1 = os.path.join(dirTemp, "intersect_t1.shp")
        intersect_temp2 = os.path.join(dirTemp, "intersect_t2.shp")
        multisingle_temp1 = os.path.join(dirTemp, "multisingle_t1.shp")
        multisingle_temp2 = os.path.join(dirTemp, "multisingle_t2.shp") 
        select_temp1 = os.path.join(dirTemp, "select_t1.shp")
        select_temp2 = os.path.join(dirTemp, "select_t2.shp")
        symdif12 = os.path.join(dirTemp, "symdif12.shp")
        ms_symdif12 = os.path.join(dirTemp, "mu_symdif12.shp")
        #symdif12_fix = os.path.join(dirTemp, "symdif12_fix.shp")
        pref12 = os.path.join(dirTemp, "pref12.shp")
        prefBuff12 = os.path.join(dirTemp, "prefBuff12.shp")
        fishnet_temp3 = os.path.join(dirTemp, "fishnet_t3.shp")
        
        if self.m3:
            intersect_temp3 = os.path.join(dirTemp, "intersect_t3.shp")
            multisingle_temp3 = os.path.join(dirTemp, "multisingle_t3.shp")
            select_temp3 = os.path.join(dirTemp, "select_t3.shp")
            symdif23 = os.path.join(dirTemp, "symdif23.shp")
            pref23 = os.path.join(dirTemp, "pref23.shp")
            prefBuff23 = os.path.join(dirTemp, "prefBuff23.shp")
            ms_symdif23 = os.path.join(dirTemp, "mu_symdif23.shp")
            symdif13 = os.path.join(dirTemp, "symdif13.shp")
            pref13 = os.path.join(dirTemp, "pref13.shp")
            prefBuff13 = os.path.join(dirTemp, "prefBuff13.shp")
            ms_symdif13 = os.path.join(dirTemp, "mu_symdif13.shp")
            fishnet_temp4 = os.path.join(dirTemp, "fishnet_t4.shp")
            fishnet_temp5 = os.path.join(dirTemp, "fishnet_5.shp")
        
        
        # Functions
        def create_fishnet(inputSRC,outputGridfn,gridHeight,gridWidth):

            inLayer = QgsVectorLayer(inputSRC, "input", "ogr")
            extent = inLayer.extent()
            srsSource = inLayer.crs()
            xmin = float(extent.xMinimum())
            xmax = float(extent.xMaximum())
            ymin = float(extent.yMinimum())
            ymax = float(extent.yMaximum())

            gridWidth = float(gridWidth)
            gridHeight = float(gridHeight)

            rows = ceil((ymax-ymin)/gridHeight)
            cols = ceil((xmax-xmin)/gridWidth)

            ringXleftOrigin = xmin
            ringXrightOrigin = xmin + gridWidth
            ringYtopOrigin = ymin
            ringYbottomOrigin = ymin+gridHeight


            outDataSource = QgsVectorLayer("Polygon?crs="+srsSource.authid(), "temp", "memory")
            outDataSource.startEditing()

            countcols = 0
            while countcols < cols:
                countcols += 1

                ringYtop = ringYtopOrigin
                ringYbottom =ringYbottomOrigin
                countrows = 0

                while countrows < rows:
                    countrows += 1

                    points = [QgsPointXY(ringXleftOrigin, ringYbottom), QgsPointXY(ringXrightOrigin, ringYbottom), QgsPointXY(ringXrightOrigin, ringYtop), QgsPointXY(ringXleftOrigin, ringYtop), QgsPointXY(ringXleftOrigin, ringYbottom)]
                    polygon = QgsGeometry.fromPolygonXY([points])

                    feature = QgsFeature()
                    feature.setGeometry(polygon)

                    outDataSource.addFeature(feature)

                    ringYtop = ringYtop + gridHeight
                    ringYbottom = ringYbottom + gridHeight

                ringXleftOrigin = ringXleftOrigin + gridWidth
                ringXrightOrigin = ringXrightOrigin + gridWidth


            outDataSource.commitChanges()

            QgsVectorFileWriter.writeAsVectorFormat(outDataSource, outputGridfn, "UTF-8", outDataSource.crs(), "ESRI Shapefile")
            outDataSource = None

            return(outputGridfn, srsSource.authid())

        def clip_shp(sourceFeature, clipFeature, outputFeature):
            inlyr_src = QgsVectorLayer(sourceFeature, 'temp', 'ogr')
            inlyr_clip = QgsVectorLayer(clipFeature, 'temp', 'ogr')
            processing.run("qgis:clip", {"INPUT": inlyr_src, "OVERLAY": inlyr_clip, "OUTPUT": outputFeature})

            del inlyr_src, inlyr_clip
            return(outputFeature)

        def intersect_simple(input_shp, overlay, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:intersection", {"INPUT": input_shp, "OVERLAY": overlay, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)

        def multi_to_single(input_shp, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:multiparttosingleparts", {"INPUT": input_shp, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)

        def update_field_area(input_shp, field_area):
            
            layer = QgsVectorLayer(input_shp, "input", "ogr")

            if not layer.fields().indexFromName(field_area) >= 0:
                layer.dataProvider().addAttributes([QgsField(field_area, QVariant.Double, len = 20, prec=4)])
                layer.updateFields()

            layer.startEditing()

            for feature in layer.getFeatures():
                area = feature.geometry().area()
                layer.changeAttributeValue(feature.id(), layer.fields().indexFromName(field_area), area)

            layer.commitChanges()

        def update_field_fid(input_shp, field_fid):

            layer = QgsVectorLayer(input_shp, "input", "ogr")
            layer.startEditing()

            count = 1
            for feature in layer.getFeatures():
                layer.changeAttributeValue(feature.id(), layer.fields().indexFromName(field_fid), count)
                count+=1

            layer.commitChanges()

        def select_att(input_shp, field, operator, value, output_shp):

            select = ("{field1}{operator1}{value1}").format(
                field1 = field,
                operator1 = operator,
                value1 = value
            )

            layer = QgsVectorLayer(input_shp, "input", "ogr")

            expression = QgsExpression(select)
            request = QgsFeatureRequest(expression)

            layer.removeSelection()

            matching_features = layer.getFeatures(request)
            feature_ids = [feature.id() for feature in matching_features]
            layer.selectByIds(feature_ids)


            new_layer = QgsVectorLayer("Polygon?crs=" + layer.crs().authid(), "Selected Features", "memory")
            new_layer_data = new_layer.dataProvider()

            new_layer_data.addAttributes(layer.fields())
            new_layer.updateFields()

            for feature in layer.getSelectedFeatures():
                new_layer_data.addFeature(feature)

            QgsVectorFileWriter.writeAsVectorFormat(new_layer, output_shp, "UTF-8", layer.crs(), "ESRI Shapefile")

            layer.removeSelection()
            
            return(output_shp)

        def stats_by_AreaFID(input_shp, field_AREA, field_FID, field_AreaFinal, field_CountFinal):

            fidUnique = []
            sumA = []
            countA = []

            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            fc = inlyr.featureCount()

            list_f = []
            for i in range(0, fc):
                feat = inlyr.getFeature(i)
                fid_value = feat[field_FID]
                list_f.append(fid_value)
            set_fid = set(list_f)

            for f in set_fid:
                fidUnique.append(f)
                select = ('"{field1}" {operator1} {value1}').format(
                    field1 = field_FID,
                    operator1 = '=',
                    value1 = f
                )
                inlyr.selectByExpression(select)
                count = inlyr.selectedFeatureCount()
                countA.append(count)

                ids = inlyr.selectedFeatureIds()
                request = QgsFeatureRequest()
                request.setFilterFids(ids)
                temp_a = []
                for feature in inlyr.getFeatures(request):
                    reg = feature[field_AREA]
                    temp_a.append(reg)
                    del reg
                del ids, request, count
                sumA.append(sum(temp_a))

            dict_fid = {field_FID: fidUnique, field_AreaFinal: sumA, field_CountFinal: countA}
            inlyr = None
            del inlyr
            return(dict_fid)

        def create_field(input_shp, field_name, field_type, field_precision, field_width):

            if field_type == 0:
                new_field = QgsField(field_name, QVariant.Int, len = field_width)
            elif field_type == 1:
                new_field = QgsField(field_name, QVariant.Double, len = field_width, prec = field_precision)
            elif field_type == 2:
                new_field = QgsField(field_name, QVariant.String, len = field_width)

            layer = QgsVectorLayer(input_shp, "input", "ogr")

            if not layer.fields().indexFromName(field_name) >= 0:
                
                layer.dataProvider().addAttributes([new_field])
                layer.updateFields()

        def join_FID_AREA(input_shp, input_dict, join_field, field_COUNT, field_AREA):

            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            fc = len(input_dict[join_field])

            for f in range(0, fc):

                COUNT_value = input_dict[field_COUNT][f]
                AREA_value = input_dict[field_AREA][f]
                select = ('"{field1}" {operator1} {value1}').format(
                    field1 = join_field,
                    operator1 = '=',
                    value1 = input_dict[join_field][f]
                )
                inlyr.selectByExpression(select)

                # get the list of selected ids
                ids = inlyr.selectedFeatureIds()

                # create the request with the selected ids
                request = QgsFeatureRequest()
                request.setFilterFids(ids)

                for feature in inlyr.getFeatures(request):
                    inlyr.startEditing()
                    feature.setAttribute(feature.fieldNameIndex(field_COUNT), COUNT_value)
                    feature.setAttribute(feature.fieldNameIndex(field_AREA), AREA_value)
                    inlyr.updateFeature(feature)
                inlyr.commitChanges()
                iface.vectorLayerTools().stopEditing(inlyr)
            
            #null to 0
            inlyr.selectByExpression(('"{field1}" is NULL').format(field1 = field_COUNT))
            ids = inlyr.selectedFeatureIds()
            request = QgsFeatureRequest()
            request.setFilterFids(ids)
            for feature in inlyr.getFeatures(request):
                inlyr.startEditing()
                feature.setAttribute(feature.fieldNameIndex(field_COUNT), 0)
                feature.setAttribute(feature.fieldNameIndex(field_AREA), 0)
                inlyr.updateFeature(feature)
            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None

        def calcField(input_shp, target_field, target_value, fieldNULL, fieldCalc):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            inlyr.startEditing()
            for feature in inlyr.getFeatures():
                #inlyr.startEditing()
                #feature.setAttribute(feature.fieldNameIndex(target_field), target_value)
                #inlyr.updateFeature(feature)
                feature[target_field] = target_value

                pos = feature[fieldNULL]
                if not pos:
                    feature[fieldNULL] = feature[fieldCalc]
                    #feature.setAttribute(feature.fieldNameIndex(fieldNULL), feature.attributes()[feature.fieldNameIndex(fieldCalc)] )

                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None

        def calcField_byLocation(in_layer, intersect, target_field, target_value):
            inlyr = QgsVectorLayer(in_layer, 'temp', 'ogr')
            inlyrI = QgsVectorLayer(intersect, 'temp', 'ogr')
            processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 1, "INTERSECT": inlyrI, "METHOD": 0})

            ids = inlyr.selectedFeatureIds()
            request = QgsFeatureRequest()
            request.setFilterFids(ids)

            for feature in inlyr.getFeatures(request):
                inlyr.startEditing()
                feature.setAttribute(feature.fieldNameIndex(target_field), target_value)
                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None
            
        def calcField_bySelection_single(input_shp, select_field, select_operator, select_value, target_field, target_value):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            select = ('"{field1}" {operator1} {value1}').format(
                field1 = select_field,
                operator1 = select_operator,
                value1 = select_value
            )
            inlyr.selectByExpression(select)

            ids = inlyr.selectedFeatureIds()
            request = QgsFeatureRequest()
            request.setFilterFids(ids)

            for feature in inlyr.getFeatures(request):
                inlyr.startEditing()
                feature.setAttribute(feature.fieldNameIndex(target_field), target_value)
                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None

        def calcField_bySelection_double(input_shp, select_fields, select_operators, select_values, condicional, target_field, target_value):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            select = ('"{field1}" {operator1} {value1} {cond} "{field2}" {operator2} {value2}').format(
                field1 = select_fields[0],
                operator1 = select_operators[0],
                value1 = select_values[0],
                cond = condicional,
                field2 = select_fields[1],
                operator2 = select_operators[1],
                value2 = select_values[1]
            )
            inlyr.selectByExpression(select)

            ids = inlyr.selectedFeatureIds()
            request = QgsFeatureRequest()
            request.setFilterFids(ids)

            for feature in inlyr.getFeatures(request):
                inlyr.startEditing()
                feature.setAttribute(feature.fieldNameIndex(target_field), target_value)
                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None

        def calcField_bySelection_triple(input_shp, select_fields, select_operators, select_values, condicional, target_field, target_value):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            select = ('"{field1}" {operator1} {value1} {cond1} "{field2}" {operator2} {value2} {cond2} "{field3}" {operator3} {value3}').format(
                field1 = select_fields[0],
                operator1 = select_operators[0],
                value1 = select_values[0],
                cond1 = condicional[0],
                field2 = select_fields[1],
                operator2 = select_operators[1],
                value2 = select_values[1],
                cond2 = condicional[1],
                field3 = select_fields[2],
                operator3 = select_operators[2],
                value3 = select_values[2]
            )
            inlyr.selectByExpression(select)

            ids = inlyr.selectedFeatureIds()
            request = QgsFeatureRequest()
            request.setFilterFids(ids)

            for feature in inlyr.getFeatures(request):
                inlyr.startEditing()
                feature.setAttribute(feature.fieldNameIndex(target_field), target_value)
                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None

        def calcField_Clear_doubleSub(input_shp, calc_fields, target_field):
            
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')

            for feature in inlyr.getFeatures():
                inlyr.startEditing()
                feature.setAttribute(feature.fieldNameIndex(target_field), (feature[calc_fields[1]]-feature[calc_fields[0]]))
                inlyr.updateFeature(feature)

            inlyr.commitChanges()
            iface.vectorLayerTools().stopEditing(inlyr)
            inlyr = None

        def symmetrical_difference(input_shp, overlay, output_shp):
            inlyr = QgsVectorLayer(input_shp, 'temp', 'ogr')
            processing.run("qgis:symmetricaldifference", {"INPUT": input_shp, "OVERLAY": overlay, "OUTPUT": output_shp})
            del inlyr
            return(output_shp)
        
        def joinLocation(in_layer, join_layer, join_field, output_shp):
            inlyr = QgsVectorLayer(in_layer, 'temp', 'ogr')
            inlyrJ = QgsVectorLayer(join_layer, 'temp', 'ogr')
            #context = dataobjects.createContext()
            #context.setInvalidGeometryCheck(QgsFeatureRequest.GeometryNoCheck)
            try:
                processing.run("native:joinattributesbylocation", {"INPUT": in_layer, "JOIN": join_layer, "PREDICATE": 1, "JOIN_FIELDS": join_field, "METHOD": 1,"OUTPUT": output_shp})
            except Exception as e:
                processing.run("qgis:joinattributesbylocation", {"INPUT": in_layer, "JOIN": join_layer, "PREDICATE": 1, "JOIN_FIELDS": join_field, "METHOD": 1,"OUTPUT": output_shp})
            del inlyr, inlyrJ
            return(output_shp)
        
        def selectPref(in_layer, intersect, buff, output):
            inlyr = QgsVectorLayer(in_layer, 'temp', 'ogr')
            inlyrI = QgsVectorLayer(intersect, 'temp', 'ogr')
            processing.run('native:buffer', {"INPUT":inlyr, "DISTANCE": 0.2, "OUTPUT": buff})
            del inlyr
            inlyr = QgsVectorLayer(buff, 'temp', 'ogr')
            #processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 3, "INTERSECT": inlyrI, "METHOD": 0})
            #processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 4, "INTERSECT": inlyrI, "METHOD": 1})
            #inlyr.invertSelection()
            processing.run('qgis:selectbylocation', {"INPUT":inlyr, "PREDICATE": 6, "INTERSECT": inlyrI, "METHOD": 0})
            processing.run('native:saveselectedfeatures', {"INPUT":inlyr, "OUTPUT": output})
            #driver = ogr.GetDriverByName('ESRI Shapefile')
            #outds = driver.CreateDataSource(output)
            #outlyr = outds.CopyLayer(inlyr, output)
            del inlyr, inlyrI, buff
            #return(outlyr)
        
   
        feedback.pushInfo("This process may take a long time, please wait.")
        feedback.pushInfo("If a command window opens, do not close it. It will close automatically.")
        
        if not self.dstr:
            # Fishnet Area and CRS
            fishnet_t1 = create_fishnet(study_areaS, fishnet_temp1, squares, squares)
            feedback.pushInfo("Fishnet Created")
            
            my_crs = QgsCoordinateReferenceSystem(fishnet_t1[1])
            QgsProject.instance().setCrs(my_crs)
            QgsProject.instance().setEllipsoid("NONE")
            
            feedback.pushInfo("Clipping Fishnet...")
            study_areaS = clip_shp(fishnet_t1[0], study_areaS, fishnet_temp2)
            feedback.pushInfo("Fishnet Clipped by Study Area")
            
            # Intersect moment 1, 2 and 3
            intersect_t1 = intersect_simple(land1S, study_areaS, intersect_temp1)
            update_field_area(intersect_t1, areaInicial_field)
            feedback.pushInfo("Intersect 1 Done")

            intersect_t2 = intersect_simple(land2S, study_areaS, intersect_temp2)
            update_field_area(intersect_t2, areaInicial_field)
            feedback.pushInfo("Intersect 2 Done")
            if self.m3:
                intersect_t3 = intersect_simple(land3S, study_areaS, intersect_temp3)
                update_field_area(intersect_t3, areaInicial_field)
                feedback.pushInfo("Intersect 3 Done")
        else:
            # Create FID field
            create_field(study_areaS, Fid_Field, 0, 0, 10)
            update_field_fid(study_areaS, Fid_Field)
            
            # Districts CRS
            layer = QgsVectorLayer(study_areaS, 'temp', 'ogr')
            lyrCRS = layer.crs().authid()
            my_crs = QgsCoordinateReferenceSystem(lyrCRS)
            QgsProject.instance().setCrs(my_crs)
            QgsProject.instance().setEllipsoid("NONE")
            
            # Intersect moment 1, 2 and 3
            intersect_t1 = intersect_simple(land1S, study_areaS, intersect_temp1)
            update_field_area(intersect_t1, areaInicial_field)
            feedback.pushInfo("Intersect 1 Done")

            intersect_t2 = intersect_simple(land2S, study_areaS, intersect_temp2)
            update_field_area(intersect_t2, areaInicial_field)
            feedback.pushInfo("Intersect 2 Done")
            
            if self.m3:
                intersect_t3 = intersect_simple(land3S, study_areaS, intersect_temp3)
                update_field_area(intersect_t3, areaInicial_field)
                feedback.pushInfo("Intersect 3 Done")
            
        # Multipart to singlepart moment 1, 2 and 3
        multisingle_t1 = multi_to_single(intersect_t1, multisingle_temp1)
        update_field_area(multisingle_t1, areaInicial_field)
        feedback.pushInfo("Multi to SinglePart 1 Done")

        multisingle_t2 = multi_to_single(intersect_t2, multisingle_temp2)
        update_field_area(multisingle_t2, areaInicial_field)
        feedback.pushInfo("Multi to SinglePart 2 Done")
        
        if self.m3:
            multisingle_t3 = multi_to_single(intersect_t3, multisingle_temp3)
            update_field_area(multisingle_t3, areaInicial_field)
            feedback.pushInfo("Multi to SinglePart 3 Done")
        
        # Feature selection moment 1, 2 and 3
        select_t1 = select_att(multisingle_t1, areaInicial_field, ">=", min_size, select_temp1)
        update_field_area(select_t1, areaInicial_field)
        feedback.pushInfo("Patch 1 Done")

        select_t2 = select_att(multisingle_t2, areaInicial_field, ">=", min_size, select_temp2)
        update_field_area(select_t2, areaInicial_field)
        feedback.pushInfo("Patch 2 Done")
        
        if self.m3:
            select_t3 = select_att(multisingle_t3, areaInicial_field, ">=", min_size, select_temp3)
            update_field_area(select_t3, areaInicial_field)
            feedback.pushInfo("Patch 3 Done")
        
        # Join fields moment 1, 2 and 3
        statsSum_1 = stats_by_AreaFID(select_t1, areaInicial_field, Fid_Field, area_1, freq_field_1)
        create_field(study_areaS, freq_field_1, 0, 0, 32)
        create_field(study_areaS, area_1, 1, 6, 32)
        join_FID_AREA(study_areaS, statsSum_1, Fid_Field, freq_field_1, area_1)
        feedback.pushInfo("Join fields 1 Done")

        statsSum_2 = stats_by_AreaFID(select_t2, areaInicial_field, Fid_Field, area_2, freq_field_2)
        create_field(study_areaS, freq_field_2, 0, 0, 32)
        create_field(study_areaS, area_2, 1, 6, 32)
        join_FID_AREA(study_areaS, statsSum_2, Fid_Field, freq_field_2, area_2)
        feedback.pushInfo("Join fields 2 Done")
        
        if self.m3:
            statsSum_3 = stats_by_AreaFID(select_t3, areaInicial_field, Fid_Field, area_3, freq_field_3)
            create_field(study_areaS, freq_field_3, 0, 0, 32)
            create_field(study_areaS, area_3, 1, 6, 32)
            join_FID_AREA(study_areaS, statsSum_3, Fid_Field, freq_field_3, area_3)
            feedback.pushInfo("Join fields 3 Done")

        # Symmetrical Difference moment 1, 2 and 3
        symmetrical_difference(select_t1, select_t2, symdif12)
        feedback.pushInfo("Symmetrical Difference 12 Done")
        multisingle_symdif12 = multi_to_single(symdif12, ms_symdif12)
        feedback.pushInfo("Multi to SinglePart 12 Done")
        create_field(multisingle_symdif12, sym12, 0, 0, 1)
        calcField(multisingle_symdif12, sym12, 1, Fid_Field, "FID_2")
        #processing.run('native:fixgeometries', {"INPUT":multisingle_symdif12, "OUTPUT": symdif12_fix})
        
        if pref:
            feedback.pushInfo("Perforation 12 check")
            selectPref(multisingle_symdif12, select_t1, prefBuff12, pref12)

        fishnet_t3 = joinLocation(study_areaS, multisingle_symdif12, sym12, fishnet_temp3)

        if self.m3:
            symmetrical_difference(select_t2, select_t3, symdif23)
            feedback.pushInfo("Symmetrical Difference 23 Done")
            multisingle_symdif23 = multi_to_single(symdif23, ms_symdif23)
            feedback.pushInfo("Multi to SinglePart 23 Done")
            create_field(multisingle_symdif23, sym23, 0, 0, 1)
            calcField(multisingle_symdif23, sym23, 1, Fid_Field, "FID_2")
            
            if pref:
                feedback.pushInfo("Perforation 23 check")
                selectPref(multisingle_symdif23, select_t2, prefBuff23, pref23)
                
            fishnet_t4 = joinLocation(fishnet_t3, multisingle_symdif23, sym23, fishnet_temp4)
            
            symmetrical_difference(select_t1, select_t3, symdif13)
            feedback.pushInfo("Symmetrical Difference 13 Done")
            multisingle_symdif13 = multi_to_single(symdif13, ms_symdif13)
            feedback.pushInfo("Multi to SinglePart 13 Done")
            create_field(multisingle_symdif13, sym13, 0, 0, 1)
            calcField(multisingle_symdif13, sym13, 1, Fid_Field, "FID_2")
            
            if pref:
                feedback.pushInfo("Perforation 13 check")
                selectPref(multisingle_symdif13, select_t1, prefBuff13, pref13)
                
            fishnet_t5 = joinLocation(fishnet_t4, multisingle_symdif13, sym13, fishnet_temp5)
            
        # Fishnet evaluation
        feedback.pushInfo("Finishing the process, please wait...")
        if not self.m3:
            if pref:
                create_field(fishnet_t3, perforation12, 0, 0, 5)
            create_field(fishnet_t3, presence_1, 0, 0, 5)
            create_field(fishnet_t3, presence_2, 0, 0, 5)
            create_field(fishnet_t3, var_area_12, 1, 6, 32)
            create_field(fishnet_t3, var_NP_12, 0, 0, 32)
            create_field(fishnet_t3, ToD_12, 2, 0, 50)
            
            if pref:
                calcField_byLocation(fishnet_t3, pref12, perforation12, 1)
            calcField_bySelection_single(fishnet_t3, freq_field_1, ">", 0, presence_1, 1)
            calcField_bySelection_single(fishnet_t3, freq_field_1, "=", 0, presence_1, 0)
            calcField_bySelection_single(fishnet_t3, freq_field_2, ">", 0, presence_2, 1)
            calcField_bySelection_single(fishnet_t3, freq_field_2, "=", 0, presence_2, 0)

            calcField_Clear_doubleSub(fishnet_t3, [area_1, area_2], var_area_12)
            calcField_Clear_doubleSub(fishnet_t3, [freq_field_1, freq_field_2], var_NP_12)
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_area_12], [">=", "<="], [-100, 100], "AND", var_area_12, 0)

            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], ["=", "="], [0, 0], "AND", ToD_12, "A - No change")
            calcField_bySelection_triple(fishnet_t3, [var_area_12, var_NP_12, sym12], ["=", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_12, "A1 - Spatial shift")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], ["=", ">"], [0, 0], "AND", ToD_12, "B - Fragmentation per se")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], ["=", "<"], [0, 0], "AND", ToD_12, "C - Aggregation per se")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], [">", "="], [0, 0], "AND", ToD_12, "D - Gain")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], ["<", "="], [0, 0], "AND", ToD_12, "E - Loss")
            if pref:
                calcField_bySelection_triple(fishnet_t3, [var_area_12, var_NP_12, perforation12], ["<", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_12, "E1 - Perforation")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], [">", ">"], [0, 0], "AND", ToD_12, "F - NP increment by gain")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], [">", "<"], [0, 0], "AND", ToD_12, "G - Aggregation by gain")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], ["<", "<"], [0, 0], "AND", ToD_12, "H - NP decrement by loss")
            calcField_bySelection_double(fishnet_t3, [var_area_12, var_NP_12], ["<", ">"], [0, 0], "AND", ToD_12, "I - Fragmentation by loss")
            calcField_bySelection_double(fishnet_t3, [presence_1, presence_2], ["=", "="], [0, 0], "AND", ToD_12, "Study object is absent")
            
            if forec:
                create_field(fishnet_t3, forecast, 2, 0, 50)
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'A - No change'", forecast, 'A - No change')
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'A1 - Spatial shift'", forecast, 'A - No change')
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'G - Aggregation by gain'", forecast, 'TOTAL COVER')
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'F - NP increment by gain'", forecast, 'G then TOTAL COVER')
                calcField_bySelection_double(fishnet_t3, [ToD_12, freq_field_2], ["=", "="], ["'D - Gain'", 1], "AND", forecast, 'TOTAL COVER')
                calcField_bySelection_double(fishnet_t3, [ToD_12, freq_field_2], ["=", ">"], ["'D - Gain'", 1], "AND", forecast, 'G then TOTAL COVER')
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'H - NP decrement by loss'", forecast, 'NO COVER')
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'I - Fragmentation by loss'", forecast, 'H then NO COVER')
                calcField_bySelection_double(fishnet_t3, [ToD_12, freq_field_2], ["=", "="], ["'E - Loss'", 1], "AND", forecast, 'NO COVER')
                calcField_bySelection_double(fishnet_t3, [ToD_12, freq_field_2], ["=", ">"], ["'E - Loss'", 1], "AND", forecast, 'H then NO COVER')
                calcField_bySelection_single(fishnet_t3, ToD_12, "=", "'Study object is absent'", forecast, 'No forecast')


            layer = QgsVectorLayer(fishnet_temp3, "", "ogr")
        else:
            if pref:
                create_field(fishnet_t5, perforation12, 0, 0, 5)
                create_field(fishnet_t5, perforation23, 0, 0, 5)
                create_field(fishnet_t5, perforation13, 0, 0, 5)
            create_field(fishnet_t5, presence_1, 0, 0, 5)
            create_field(fishnet_t5, presence_2, 0, 0, 5)
            create_field(fishnet_t5, presence_3, 0, 0, 5)
            create_field(fishnet_t5, var_area_12, 1, 6, 32)
            create_field(fishnet_t5, var_area_23, 1, 6, 32)
            create_field(fishnet_t5, var_area_13, 1, 6, 32)
            create_field(fishnet_t5, var_NP_12, 0, 0, 32)
            create_field(fishnet_t5, var_NP_23, 0, 0, 32)
            create_field(fishnet_t5, var_NP_13, 0, 0, 32)
            create_field(fishnet_t5, ToD_12, 2, 0, 50)
            create_field(fishnet_t5, ToD_23, 2, 0, 50)
            create_field(fishnet_t5, ToD_13, 2, 0, 50)

            if pref:
                calcField_byLocation(fishnet_t5, pref12, perforation12, 1)
                calcField_byLocation(fishnet_t5, pref23, perforation23, 1)
                calcField_byLocation(fishnet_t5, pref13, perforation13, 1)
            calcField_bySelection_single(fishnet_t5, freq_field_1, ">", 0, presence_1, 1)
            calcField_bySelection_single(fishnet_t5, freq_field_1, "=", 0, presence_1, 0)
            calcField_bySelection_single(fishnet_t5, freq_field_2, ">", 0, presence_2, 1)
            calcField_bySelection_single(fishnet_t5, freq_field_2, "=", 0, presence_2, 0)
            calcField_bySelection_single(fishnet_t5, freq_field_3, ">", 0, presence_3, 1)
            calcField_bySelection_single(fishnet_t5, freq_field_3, "=", 0, presence_3, 0)

            calcField_Clear_doubleSub(fishnet_t5, [area_1, area_2], var_area_12)
            calcField_Clear_doubleSub(fishnet_t5, [area_2, area_3], var_area_23)
            calcField_Clear_doubleSub(fishnet_t5, [area_1, area_3], var_area_13)
            calcField_Clear_doubleSub(fishnet_t5, [freq_field_1, freq_field_2], var_NP_12)
            calcField_Clear_doubleSub(fishnet_t5, [freq_field_2, freq_field_3], var_NP_23)
            calcField_Clear_doubleSub(fishnet_t5, [freq_field_1, freq_field_3], var_NP_13)
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_area_12], [">=", "<="], [-100, 100], "AND", var_area_12, 0)
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_area_23], [">=", "<="], [-100, 100], "AND", var_area_23, 0)
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_area_13], [">=", "<="], [-100, 100], "AND", var_area_13, 0)

            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], ["=", "="], [0, 0], "AND", ToD_12, "A - No change")
            calcField_bySelection_triple(fishnet_t5, [var_area_12, var_NP_12, sym12], ["=", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_12, "A1 - Spatial shift")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], ["=", ">"], [0, 0], "AND", ToD_12, "B - Fragmentation per se")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], ["=", "<"], [0, 0], "AND", ToD_12, "C - Aggregation per se")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], [">", "="], [0, 0], "AND", ToD_12, "D - Gain")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], ["<", "="], [0, 0], "AND", ToD_12, "E - Loss")
            if pref:
                calcField_bySelection_triple(fishnet_t5, [var_area_12, var_NP_12, perforation12], ["<", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_12, "E1 - Perforation")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], [">", ">"], [0, 0], "AND", ToD_12, "F - NP increment by gain")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], [">", "<"], [0, 0], "AND", ToD_12, "G - Aggregation by gain")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], ["<", "<"], [0, 0], "AND", ToD_12, "H - NP decrement by loss")
            calcField_bySelection_double(fishnet_t5, [var_area_12, var_NP_12], ["<", ">"], [0, 0], "AND", ToD_12, "I - Fragmentation by loss")
            calcField_bySelection_double(fishnet_t5, [presence_1, presence_2], ["=", "="], [0, 0], "AND", ToD_12, "Study object is absent")
            
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], ["=", "="], [0, 0], "AND", ToD_23, "A - No change")
            calcField_bySelection_triple(fishnet_t5, [var_area_23, var_NP_23, sym23], ["=", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_23, "A1 - Spatial shift")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], ["=", ">"], [0, 0], "AND", ToD_23, "B - Fragmentation per se")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], ["=", "<"], [0, 0], "AND", ToD_23, "C - Aggregation per se")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], [">", "="], [0, 0], "AND", ToD_23, "D - Gain")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], ["<", "="], [0, 0], "AND", ToD_23, "E - Loss")
            if pref:
                calcField_bySelection_triple(fishnet_t5, [var_area_23, var_NP_23, perforation23], ["<", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_23, "E1 - Perforation")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], [">", ">"], [0, 0], "AND", ToD_23, "F - NP increment by gain")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], [">", "<"], [0, 0], "AND", ToD_23, "G - Aggregation by gain")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], ["<", "<"], [0, 0], "AND", ToD_23, "H - NP decrement by loss")
            calcField_bySelection_double(fishnet_t5, [var_area_23, var_NP_23], ["<", ">"], [0, 0], "AND", ToD_23, "I - Fragmentation by loss")
            calcField_bySelection_double(fishnet_t5, [presence_2, presence_3], ["=", "="], [0, 0], "AND", ToD_23, "Study object is absent")
            
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], ["=", "="], [0, 0], "AND", ToD_13, "A - No change")
            calcField_bySelection_triple(fishnet_t5, [var_area_13, var_NP_13, sym13], ["=", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_13, "A1 - Spatial shift")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], ["=", ">"], [0, 0], "AND", ToD_13, "B - Fragmentation per se")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], ["=", "<"], [0, 0], "AND", ToD_13, "C - Aggregation per se")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], [">", "="], [0, 0], "AND", ToD_13, "D - Gain")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], ["<", "="], [0, 0], "AND", ToD_13, "E - Loss")
            if pref:
                calcField_bySelection_triple(fishnet_t5, [var_area_13, var_NP_13, perforation13], ["<", "=", "="], [0, 0, 1], ["AND", "AND"], ToD_13, "E1 - Perforation")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], [">", ">"], [0, 0], "AND", ToD_13, "F - NP increment by gain")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], [">", "<"], [0, 0], "AND", ToD_13, "G - Aggregation by gain")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], ["<", "<"], [0, 0], "AND", ToD_13, "H - NP decrement by loss")
            calcField_bySelection_double(fishnet_t5, [var_area_13, var_NP_13], ["<", ">"], [0, 0], "AND", ToD_13, "I - Fragmentation by loss")
            calcField_bySelection_double(fishnet_t5, [presence_1, presence_3], ["=", "="], [0, 0], "AND", ToD_13, "Study object is absent")
            
            if forec:
                create_field(fishnet_t5, forecast, 2, 0, 50)
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'A - No change'", forecast, 'A - No change')
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'A1 - Spatial shift'", forecast, 'A - No change')
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'G - Aggregation by gain'", forecast, 'TOTAL COVER')
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'F - NP increment by gain'", forecast, 'G then TOTAL COVER')
                calcField_bySelection_double(fishnet_t5, [ToD_23, freq_field_3], ["=", "="], ["'D - Gain'", 1], "AND", forecast, 'TOTAL COVER')
                calcField_bySelection_double(fishnet_t5, [ToD_23, freq_field_3], ["=", ">"], ["'D - Gain'", 1], "AND", forecast, 'G then TOTAL COVER')
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'H - NP decrement by loss'", forecast, 'NO COVER')
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'I - Fragmentation by loss'", forecast, 'H then NO COVER')
                calcField_bySelection_double(fishnet_t5, [ToD_23, freq_field_3], ["=", "="], ["'E - Loss'", 1], "AND", forecast, 'NO COVER')
                calcField_bySelection_double(fishnet_t5, [ToD_23, freq_field_3], ["=", ">"], ["'E - Loss'", 1], "AND", forecast, 'H then NO COVER')
                calcField_bySelection_single(fishnet_t5, ToD_23, "=", "'Study object is absent'", forecast, 'No forecast')
                
            layer = QgsVectorLayer(fishnet_temp5, "", "ogr")
            
        # Finish process
        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            layer.fields(),
            layer.wkbType(),
            layer.sourceCrs()
        )
        
        for f in layer.getFeatures():
            sink.addFeature(f, QgsFeatureSink.FastInsert)
           
        QgsProject.instance().removeMapLayer(layer.id())
        if not self.dstr:
            del fishnet_t1
        del layer, study_areaS, fishnet_t3
        if self.m3:
            del fishnet_t4, fishnet_t5
        
        
        for root, dirs, files in os.walk(dirTemp):
            for file in files:
                path = os.path.join(dirTemp, file)
                os.remove(path)
        os.rmdir(dirTemp)
        
        self.dest_id=dest_id
        
        return {self.OUTPUT: dest_id}
        
    def postProcessAlgorithm(self, context, feedback):
        output = QgsProcessingUtils.mapLayerFromString(self.dest_id, context)
        path=ProcessingConfig.getSetting('SCRIPTS_FOLDERS') + '\LDT4QGIS_2M_symbology.qml'
        if self.m3:
            path=ProcessingConfig.getSetting('SCRIPTS_FOLDERS') + '\LDT4QGIS_3M_symbology.qml'
        output.loadNamedStyle(path)
        output.triggerRepaint()
        return {self.OUTPUT: self.dest_id}